package at.zie.Engine;

public abstract class AbstractMotor implements Motor {
	private int serial;

	public AbstractMotor(int serial) {
		super();
		this.serial = serial;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}
	
	
}

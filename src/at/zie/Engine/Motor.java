package at.zie.Engine;

public interface Motor {
	public void run(int amount);
	public int getSerial();
}
